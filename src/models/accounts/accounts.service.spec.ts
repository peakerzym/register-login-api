import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { Accounts } from './accounts.model';
import { ModelAccountsService } from './accounts.service';

const mockAccounts = {
  _id: new Types.ObjectId('65884a39cca1faf998023f15'),
  code: 'AC31265',
  firstname: 'Suttipong',
  lastname: 'buahom',
  email: 'gappa_papa@msn.com',
  password: 'U2FsdGVkX1+7aL2PWVMxRAw1ZhDLOVvq/yM1uLhYXlE=',
  contriesCode: 'TH',
  lastLogin: new Date('2023-12-25T07:30:57.157Z'),
  createdAt: new Date('2023-12-24T15:11:53.174Z'),
  updatedAt: new Date('2023-12-25T07:30:57.158Z')
};

describe('AccountService', () => {
  let modelAccountsService: ModelAccountsService;
  let accountsModel: ReturnModelType<typeof Accounts>

  const accountsObject = {
    _id: new Types.ObjectId('65884a39cca1faf998023f15'),
    code: 'AC31265',
    firstname: 'Suttipong',
    lastname: 'buahom',
    email: 'gappa_papa@msn.com',
    password: 'U2FsdGVkX1+TwBgbW/L0zgg5QYl3gbDhVUwBoOXO/Z0=',
    contriesCode: 'TH',
    lastLogin: new Date('2023-12-25T08:40:52.143Z'),
    createdAt: new Date('2023-12-24T15:11:53.174Z'),
    updatedAt: new Date('2023-12-25T08:40:52.144Z'),
    countries: {
      _id: new Types.ObjectId('6587f294838295cefd7a2f91'),
      code: 'TH',
      name: 'Thailand',
      createdAt: new Date('2023-12-24T08:57:56.267Z'),
      updatedAt: new Date('2023-12-24T08:57:56.267Z')
    }
  }

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        ModelAccountsService,
        {
          provide: getModelToken(Accounts.name),
          useValue: {
            create: jest.fn(),
            findOne: jest.fn(),
            populate: jest.fn()
          },
        }
      ],
    }).compile();

    modelAccountsService = app.get<ModelAccountsService>(ModelAccountsService);
    accountsModel = app.get<ReturnModelType<typeof Accounts>>(getModelToken(Accounts.name));
  });

  it('should create new accounts', async () => {
    jest.spyOn(accountsModel, 'create').mockImplementationOnce(() =>
      Promise.resolve({
        _id: new Types.ObjectId('65884a39cca1faf998023f15'),
        code: 'AC31265',
        firstname: 'Suttipong',
        lastname: 'buahom',
        email: 'gappa_papa@msn.com',
        password: 'U2FsdGVkX1+7aL2PWVMxRAw1ZhDLOVvq/yM1uLhYXlE=',
        contriesCode: 'TH',
        lastLogin: new Date('2023-12-25T07:30:57.157Z'),
        createdAt: new Date('2023-12-24T15:11:53.174Z'),
        updatedAt: new Date('2023-12-25T07:30:57.158Z')
      } as any),
    );
    const newAccounts = await modelAccountsService.create({
      contriesCode:'TH',
      firstname:'Suttipong',
      lastname:'buahom',
      email:'gappa_papa@msn.com',
      password:'123456'
    })
    expect(newAccounts).toEqual(mockAccounts);
  });


  it('should return one accounts', async () => {

    jest.spyOn(accountsModel, 'findOne').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(accountsObject),
    } as any);

    let email = {email:'gappa_papa@msn.com'}

    const cats = await modelAccountsService.findOne(email);
    expect(cats).toEqual(accountsObject);

  });

});
