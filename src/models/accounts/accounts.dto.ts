import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsIn, IsNotEmpty, IsString, IsStrongPassword } from 'class-validator';
import configuration from 'src/config/configuration';

export class CreateAccountsDto {

  @ApiHideProperty()
  code?:string

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  firstname: string

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  lastname: string

  @ApiProperty()
  @IsEmail()
  @IsString()
  email: string

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsStrongPassword()
  password: string

  @ApiProperty({ 

  })
  @IsIn(configuration().countriesCode)
  @IsNotEmpty()
  @IsString()
  contriesCode: string

}

export class AuthenticateAccountsDto {

  @ApiProperty()
  @IsEmail()
  @IsString()
  email:string

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password:string

}