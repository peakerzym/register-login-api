import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { CreateAccountsDto } from "./accounts.dto";
import { ReturnModelType } from "@typegoose/typegoose";
import { Accounts } from "./accounts.model";
import { FilterQuery, Types } from "mongoose";

@Injectable()
export class ModelAccountsService {
  constructor(
    @InjectModel(Accounts.name) 
    private readonly accountsModel: ReturnModelType<typeof Accounts>
  ) {}

  async create(createAccountsDto: CreateAccountsDto): Promise<Accounts> {
    /*const createdAccounts = new this.accountsModel(createAccountsDto)
    return await createdAccounts.save()
    */  

    const createdAccounts = await this.accountsModel.create(createAccountsDto);
    return createdAccounts;
  }

  async findOneById(id:Types.ObjectId):Promise<Accounts> {
    return await this.accountsModel
      .findById(id)
      .populate({ path: 'countries'})
      .lean()
  }

  async findOne( filter: FilterQuery<Accounts>):Promise<Accounts> {
    return await this.accountsModel.findOne(filter, null, {
      populate: { path: 'countries'}, 
      lean:true
    }).exec()
  }

  async updateLastLoginAndEncryptPassword(code:string, encryptPassword:string) {
    await this.accountsModel.updateOne(
      { code },
      { 
        lastLogin: Date.now(),
        password:encryptPassword
      }
    ).orFail(() => Error('function updateLastLoginAndEncryptPassword Fail')).exec()
  }

  async countAll():Promise<number> {
    return await this.accountsModel.countDocuments().exec()
  }

}