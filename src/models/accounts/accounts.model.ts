import { modelOptions, prop } from "@typegoose/typegoose";
import configuration from "src/config/configuration";
import { Countries } from "../countries/countries.model";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

export class Accounts extends TimeStamps {

  @prop({ 
    required: true,
    unique: true 
  })
  code:string
  
  @prop({ required: true })
  firstname:string
  
  @prop({ required: true })
  lastname: string
  
  @prop({ required: true })
  email:string
  
  @prop({ required: true })
  password:string
  
  @prop({
    required: true,
    enum:configuration().countriesCode
  })
  contriesCode:string

  @prop({default:Date.now()})
  lastLogin:Date

  @prop({ 
    ref: () => Countries,
    foreignField: 'code', 
    localField: 'contriesCode',
    justOne:true
  })
  countries: Countries
  
}