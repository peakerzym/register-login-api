import { Module } from "@nestjs/common";
import { ModelAccountsService } from "./accounts.service";
import { TypegooseModule } from "nestjs-typegoose";
import { Accounts } from "./accounts.model";

@Module({
  imports: [
    TypegooseModule.forFeature([ Accounts ])
  ],
  providers: [ModelAccountsService],
  exports: [ModelAccountsService]
})
export class ModelAccountsModule {}