import { modelOptions, prop } from "@typegoose/typegoose";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true ,
    versionKey: false 
  } 
})
export class Countries {

  @prop()
  code: string;

  @prop()
  name: string;

}