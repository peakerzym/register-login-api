export const mockCountries = {
  find: jest.fn().mockImplementationOnce(sort => sort(null, true)),
  sort: jest.fn()
}