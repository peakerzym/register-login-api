import { Test, TestingModule } from '@nestjs/testing';
import { ModelCountriesService } from './countries.service';
import { getModelToken } from 'nestjs-typegoose';
import { Countries } from './countries.model';
import { ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';

describe('CountriesService', () => {
  let modelCountriesService: ModelCountriesService;
  let countriesModel: ReturnModelType<typeof Countries>

  const CountriesArray = [
    {
      _id: new Types.ObjectId('6587f294838295cefd7a2eb2'),
      code: 'AF',
      name: 'Afghanistan',
      createdAt: new Date('2023-12-24T08:57:56.260Z'),
      updatedAt: new Date('2023-12-24T08:57:56.260Z')
    },
    {
      _id: new Types.ObjectId('6587f294838295cefd7a2eb3'),
      code: 'AX',
      name: 'Aland Islands',
      createdAt: new Date('2023-12-24T08:57:56.261Z'),
      updatedAt: new Date('2023-12-24T08:57:56.261Z')
    },
    {
      _id: new Types.ObjectId('6587f294838295cefd7a2eb4'),
      code: 'AL',
      name: 'Albania',
      createdAt: new Date('2023-12-24T08:57:56.261Z'),
      updatedAt: new Date('2023-12-24T08:57:56.261Z')
    }
  ]

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        ModelCountriesService,
        {
          provide: getModelToken(Countries.name),
          useValue: {
            find: jest.fn(),
            exec: jest.fn(),
          },
        }
      ],
    }).compile();

    modelCountriesService = app.get<ModelCountriesService>(ModelCountriesService);
    countriesModel = app.get<ReturnModelType<typeof Countries>>(getModelToken(Countries.name));
  });



    it('should return all countries', async () => {

      jest.spyOn(countriesModel, 'find').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(CountriesArray),
      } as any);

      const countries = await modelCountriesService.findAll();
      expect(countries).toEqual(CountriesArray);

    });

  
});
