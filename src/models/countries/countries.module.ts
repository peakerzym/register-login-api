import { Module } from "@nestjs/common";
import { ModelCountriesService } from "./countries.service";
import { TypegooseModule } from "nestjs-typegoose";
import { Countries } from "./countries.model";

@Module({
  imports: [
    TypegooseModule.forFeature([ Countries ])
  ],
  providers: [ModelCountriesService],
  exports: [ModelCountriesService]
})
export class ModelCountriesModule {}