import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CountriesDto {
  @ApiProperty()
  code: number

  @ApiProperty()
  name: string
}