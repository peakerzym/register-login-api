import { Inject, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { CountriesDto } from "./countries.dto";
import { Countries } from "./countries.model";
import { ReturnModelType } from "@typegoose/typegoose";

@Injectable()
export class ModelCountriesService {
  constructor(
    @InjectModel('Countries') 
    private readonly countriesModel: ReturnModelType<typeof Countries>
  ) {}

  async create(countriesDto: CountriesDto): Promise<Countries> {
    const createdCountries = new this.countriesModel(countriesDto)
    return await createdCountries.save()
  }

  async findAll(): Promise<Countries[]> {
    return await this.countriesModel.find({}, null, {sort: {name: 'asc'}}).exec()
  }
}