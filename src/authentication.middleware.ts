import {
  Injectable,
  NestMiddleware,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';
import configuration from './config/configuration';
import { ModelAccountsService } from './models/accounts/accounts.service';
import { UtilityService } from './components/utility.service';

@Injectable()
export class Authentication implements NestMiddleware {
  constructor(
    private readonly jwtService: JwtService,
    private utility:UtilityService,
    private modelAccountsService : ModelAccountsService,
  ) {}

  public async use(req: Request, res: Response, next: NextFunction) {
    
    try {
      
      if( (req.method==='GET' && req.path.match('/accounts')) ){
        if (! req.headers['authorization']) {
          throw new BadRequestException('Bad Request', { cause: new Error(), description: 'Authorization is request' })
        }

        const token = this.extractTokenFromHeader(req)
        
        let payload:{
          code:string
          key:string
        }

        try {

          payload = await this.jwtService.verifyAsync(
            token,
            {
              ignoreExpiration:false,
              secret:configuration().jwtSecert
            }
          )

        } catch (e){
          throw new UnauthorizedException('Unauthorized', { cause: new Error(), description: 'Invalid token or Token Expire' })
        }

        let dataAccounts = await this.modelAccountsService.findOne({code:payload.code})
        console.log(dataAccounts)
        if(dataAccounts===null){
          throw new BadRequestException('Bad Request', { cause: new Error(), description: 'Invalid token' })
        }
        
        let decodePass1 = await this.utility.decryptData(dataAccounts.password)
        let decodePass2 = await this.utility.decryptData(payload.key)

        if(
          payload.key===undefined 
          || payload.key!==dataAccounts.password
          || decodePass1!==decodePass2
        ) throw new BadRequestException('Bad Request', { cause: new Error(), description: 'Invalid token' })

        req.body.accountsPayload = dataAccounts

      }
      
      return next();
    } catch (e) {
      throw e;
    }
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
