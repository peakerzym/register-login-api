import { Injectable, Inject } from "@nestjs/common";
import { CACHE_MANAGER } from "@nestjs/cache-manager";
import { Cache } from "cache-manager";

@Injectable()
export class RedisService {

    private readonly redisKey = process.env.REDIS_KEY
    private readonly redisExpireTime = process.env.REDIS_EXPIRE_TIME

    constructor(
        @Inject(CACHE_MANAGER) 
        private cacheManager: Cache
    ) {}
        
    public async get(key:string): Promise<string> {
        return await this.cacheManager.get(this.redisKey.concat(key));
    }

    public async set(key: string, data: object, _expireTime?:number) {
        let expireTime = _expireTime ? _expireTime:this.redisExpireTime
        return await this.cacheManager.set(this.redisKey.concat(key), JSON.stringify(data), +expireTime);
    }

    public async del(key:string): Promise<void> {
        await this.cacheManager.del(this.redisKey.concat(key));
    }
}