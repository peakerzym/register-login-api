import { Module } from "@nestjs/common";
import { RedisService } from "./redis.service";
import { CacheModule } from "@nestjs/cache-manager";
import { RedisOptions } from "./redis.options";

@Module({
    imports: [
        CacheModule.registerAsync(RedisOptions)
    ],
    providers: [RedisService],
    exports: [RedisService]
})
export class RedisModule {}