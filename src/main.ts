import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { json, urlencoded } from 'express';
import { Logger } from '@nestjs/common';
import { FormatResponseInterceptor } from './formatResponse.interceptor';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const logger = new Logger();
const PORT = process.env.PORT;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(new FormatResponseInterceptor);
  app.setGlobalPrefix('v1');
  app.enableCors({
    origin: true,
    credentials: true
  });
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));

  const config = new DocumentBuilder()
    .setTitle('Register Login API')
    .setDescription('Register Login API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('apis', app, document);

  await app.listen(PORT);
  logger.log('Run on port ' + PORT);
}
bootstrap();
