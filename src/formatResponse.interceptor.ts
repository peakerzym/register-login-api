import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs'
import configuration from './config/configuration';

@Injectable()
export class FormatResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    let countTime = Date.now();
    return next.handle().pipe(
        map((data) => ({
            ...data,
            auditData: {
                timeStamp: dayjs( new Date().toLocaleString('en-US', { timeZone: configuration().tz }) ).format(),
                processTime: `${(Date.now() - countTime)} ms`
            }
        })),
      );
  }
}