import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MasterModule } from './apis/master/master.module';
import { AccountsModule } from './apis/accounts/accounts.module';
import { Authentication } from './authentication.middleware';
import { JwtModule } from '@nestjs/jwt';
import { ModelAccountsModule } from './models/accounts/accounts.module';
import { UtilityModule } from './components/utility.module';
import { TypegooseModule } from 'nestjs-typegoose';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({ 
      envFilePath: `.env.${process.env.NODE_ENV}`,
      isGlobal: true
    }),
    TypegooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URI'),
      }),
      inject: [ConfigService]
    }),
    JwtModule.register({ 
      global: true,
      secret: configuration().jwtSecert
    }),
    UtilityModule,
    ModelAccountsModule,
    MasterModule,
    AccountsModule
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(Authentication)
      .forRoutes({
        path: '*',
        method: RequestMethod.ALL
      })
  }
}
