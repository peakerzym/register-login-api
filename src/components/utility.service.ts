import { BadRequestException, Injectable } from '@nestjs/common';
import {AES, enc} from 'crypto-js';


@Injectable()
export class UtilityService {

  private readonly secretPass = "2e35f242a46d67eeb74aabc37d5e5d05";

  constructor(
  ){}

  public encryptData = async (text:string)  => {
    return AES.encrypt(
      text,
      this.secretPass
    ).toString()
  }

  public decryptData = async (text:string) => {
    //let bytes: CryptoJS.lib.WordArray
    try {
      let bytes = AES.decrypt(text, this.secretPass)
      return bytes.toString( enc.Utf8 )
    } catch (error) {
      throw new BadRequestException(`function decryptData Something went wrong.`)
    }
    
  }

}