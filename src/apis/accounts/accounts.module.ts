import { Module } from "@nestjs/common";
import { ModelAccountsModule } from "src/models/accounts/accounts.module";
import { AccountsController } from "./accounts.controller";
import { AccountsService } from "./accounts.service";
import { UtilityModule } from "src/components/utility.module";

@Module({
  imports: [
    UtilityModule,
    ModelAccountsModule
  ],
  controllers: [AccountsController],
  providers: [AccountsService]
})
export class AccountsModule {}