import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { UtilityService } from "src/components/utility.service";
import { AuthenticateAccountsDto, CreateAccountsDto } from "src/models/accounts/accounts.dto";
import { ModelAccountsService } from "src/models/accounts/accounts.service";
import { JwtService } from "@nestjs/jwt";
import { Accounts } from "src/models/accounts/accounts.model";

@Injectable()
export class AccountsService {

  constructor(
    private utility:UtilityService,
    private jwtService: JwtService,
    private modelAccountsService : ModelAccountsService
  ){}

  public async authenticate(body : AuthenticateAccountsDto) {
    let { 
      email, 
      password,
    } = body

    let dataAccounts = await this.modelAccountsService.findOne({email})
    
    if(dataAccounts!==null) {
      let decodePass = await this.utility.decryptData(dataAccounts.password)
      
      if(password!==decodePass){
        throw new UnauthorizedException('Invalid email or password.')
      }

      let encryptPassword = await this.utility.encryptData(password)

      await this.modelAccountsService.updateLastLoginAndEncryptPassword(dataAccounts.code, encryptPassword) 

      const payload = { code:dataAccounts.code, key:encryptPassword }
      const signOptions = { 
        expiresIn: '6h'
      }
      const token = await this.jwtService.signAsync(payload, signOptions)

      let data = {
        accountsCode:dataAccounts.code,
        token
      }

      return {data}

    }else{
      throw new UnauthorizedException('Invalid email or password.')
    }

  }

  public async getAccounts(body: {accountsPayload:Accounts}) {

    let{ accountsPayload } = body
    
    let data = {
      createdAt:accountsPayload.createdAt,
      updatedAt:accountsPayload.updatedAt,
      code: accountsPayload.code,
      firstname: accountsPayload.firstname,
      lastname: accountsPayload.lastname,
      email: accountsPayload.email,
      lastLogin: accountsPayload.lastLogin,
      countries:{
        code:accountsPayload.countries.code,
        name:accountsPayload.countries.name
      }
    }

    return { data }
  }

  public async createAccounts(body : CreateAccountsDto) {
    let {
      contriesCode,
      firstname,
      lastname,
      email,
      password
    } = body

    let dataAccounts = await this.modelAccountsService.findOne({email})

    if(dataAccounts !== null){
      throw new BadRequestException(`Email already exists.`)
    }

    let encryptPassword = await this.utility.encryptData(password)
    let code = await this.generateCustomerCode()
    let dataCreate = {
      code:code,
      firstname,
      lastname,
      email,
      contriesCode,
      password:encryptPassword 
    }
    
    let res = await this.modelAccountsService.create(dataCreate)
    
    return {
      status:"Success",
      message:"Accounts have been created"
    }
  }

  private async generateCustomerCode(){
    let num = await this.modelAccountsService.countAll()
    num = num+1
    return `AC` + `${Date.now()}`.substring(9,11) + num.toString() + `${Date.now()}`.substring(11)
  }

}