import { ModelAccountsService } from "src/models/accounts/accounts.service";
import { AccountsController } from "./accounts.controller";
import { AccountsService } from "./accounts.service";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthenticateAccountsDto, CreateAccountsDto } from "src/models/accounts/accounts.dto";
import { Accounts } from "src/models/accounts/accounts.model";

describe('MasterController', () => {
  let accountsController: AccountsController
  let accountsService:AccountsService

  const createAccountsDto: CreateAccountsDto = {
    firstname: 'Suttpong',
    lastname: 'Buahom',
    email:"email@msn.com",
    password:"123456",
    contriesCode:"TH"
  }

  const authenticateAccountsDto:AuthenticateAccountsDto = {
    email:"email@msn.com",
    password:"123456"
  }

  const accountObject = {
    createdAt: new Date('2023-12-24T15:11:53.174Z'),
    updatedAt: new Date('2023-12-25T14:30:42.414Z'),
    code: "AC31265",
    firstname: "Suttipong",
    lastname: "buahom",
    email: "gappa_papa@msn.com",
    lastLogin: new Date('2023-12-25T14:30:42.411Z'),
    countries: {
      code: "TH",
      name: "Thailand"
    }
  } as Accounts

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AccountsController],
      providers: [
        {
          provide: AccountsService,
          useValue: {
            createAccounts: jest.fn().mockResolvedValue(createAccountsDto),
            authenticate: jest.fn().mockResolvedValue({
              "accountsCode":"AC12345",
              "token":"jwttoken"
            }),
            getAccounts:jest.fn()
          },
        },
      ],
    }).compile();

    accountsController = app.get<AccountsController>(AccountsController);
    accountsService= app.get<AccountsService>(AccountsService);
  })

  describe('createAccounts()', () => {
    it('should create a new accounts', async () => {
      const createAccountsSpy = jest
        .spyOn(accountsService, 'createAccounts')
        .mockResolvedValueOnce({
          status:"Success",
          message:"Accounts have been created"
        });

      await accountsController.createAccounts(createAccountsDto);
      expect(createAccountsSpy).toHaveBeenCalledWith(createAccountsDto);
    })
  })

  describe('acountsAuthenticate()', () => {
    it('should return jwt token', async () => {
     
      let authenticate =  await accountsController.acountsAuthenticate(authenticateAccountsDto)
      expect( authenticate ).toEqual({
        "accountsCode":"AC12345",
        "token":"jwttoken"
      })
    
    })
  })

  describe('getAccounts()', () => {
    it('should return one accounts', async () => {

      const getAccountsSpy =jest.spyOn(accountsService, 'getAccounts').mockImplementationOnce(() =>
        Promise.resolve(accountObject as any)
      );

      let accounts =  accountsController.getAccounts({accountsPayload:accountObject})
      expect(accounts).resolves.toEqual({
        createdAt: new Date('2023-12-24T15:11:53.174Z'),
        updatedAt: new Date('2023-12-25T14:30:42.414Z'),
        code: "AC31265",
        firstname: "Suttipong",
        lastname: "buahom",
        email: "gappa_papa@msn.com",
        lastLogin: new Date('2023-12-25T14:30:42.411Z'),
        countries: {
          code: "TH",
          name: "Thailand"
        }
      })
      expect(getAccountsSpy).toHaveBeenCalled()
      
    
    })
  })


})
