import { Body, Controller, Get, Header, Headers, Post, UsePipes, ValidationPipe } from "@nestjs/common";
import { ApiHeader } from "@nestjs/swagger";
import { AuthenticateAccountsDto, CreateAccountsDto } from "src/models/accounts/accounts.dto";
import { AccountsService } from "./accounts.service";
import { Accounts } from "src/models/accounts/accounts.model";

@Controller({
  path: '/accounts'
})

export class AccountsController {
  constructor(private accountsService: AccountsService) {}
  
  @Post()
  @UsePipes(new ValidationPipe({transform: true}))
  async createAccounts(@Body() body : CreateAccountsDto) {
    try {
      return await this.accountsService.createAccounts(body);
    } catch (e) {
      throw e;
    }
  }

  @Post('/authenticate')
  @UsePipes(new ValidationPipe({transform: true}))
  async acountsAuthenticate(@Body() body : AuthenticateAccountsDto) {
    try {
      return await this.accountsService.authenticate(body);
    } catch (e) {
      throw e;
    }
  }

  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer Token',
  })
  @Get()
  async getAccounts(@Body() body : {accountsPayload:Accounts}) {
    try {
      return await this.accountsService.getAccounts(body);
    } catch (e) {
      throw e;
    }
  }

}
