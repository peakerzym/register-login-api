import { Module } from "@nestjs/common";
import { MasterController } from "./master.controller";
import { ModelCountriesModule } from "src/models/countries/countries.module";
import { MasterService } from "./master.service";

import { HttpModule } from "@nestjs/axios";
import { RedisModule } from "src/datastrore/redis.module";

@Module({
  imports: [
    ModelCountriesModule,
    HttpModule,
    RedisModule
  ],
  controllers: [MasterController],
  providers: [MasterService]
})
export class MasterModule {}
