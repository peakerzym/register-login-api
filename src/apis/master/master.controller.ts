import { Controller, Get, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { MasterService } from './master.service';
import { Countries } from 'src/models/countries/countries.model';

@Controller({
  path: '/master'
})
export class MasterController {
  constructor(private masterService: MasterService) {}
    
  @Get('/countries')
  async getCountries() {
    try {
      return await this.masterService.getCountries();
    } catch (e) {
      throw e;
    }
  }

  /*@Post('/countries')
  @UsePipes(new ValidationPipe({transform: true}))
  async postCountries() {
    try {
      return await this.masterService.createCountries();
    } catch (e) {
      throw e;
    }
  }*/
}

