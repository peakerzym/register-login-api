import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { RedisService } from "src/datastrore/redis.service";

import { ModelCountriesService } from "src/models/countries/countries.service";

@Injectable()
export class MasterService {

  private readonly redisKey = process.env.REDIS_KEY
  private readonly redisKeyCountries = this.redisKey.concat('countries')
    
  constructor(
    private readonly httpService: HttpService,
    private readonly redisService: RedisService,
    private modelCountriesService : ModelCountriesService,
  ){}

  public async getCountries(){

    this.redisService.del(this.redisKeyCountries);
    let strCountries =  await this.redisService.get(this.redisKeyCountries)
        
    if (strCountries !== null && strCountries !== undefined) {

      let data =  JSON.parse(strCountries)
      return {data}

    } else {

      let dataCountries = await this.modelCountriesService.findAll()

      let data = dataCountries.map(val => {
        return {
          code:val.code,
          name:val.name
        }
      })
      await this.redisService.set(this.redisKeyCountries, data);
      return {data}
      
    }
  }

  public async createCountries() {

    const config = {
      headers:{
        'x-api-key': 'kj7ppy1xqmvncrzp1o8xx049j8qlg318exds18j16y4qskpz',
        'x-secret': '5n2t1y4s5mmc01f0e1',
      }
    }

    try {

      let res = await this.httpService.axiosRef.get('https://dev-b2b-api.thaidestination.co.th/v1/content/countries', config)
      res.data.countries.map(async val=>{
        await this.modelCountriesService.create({code:val.code, name:val.name})
      })
    
    } catch (error) {
      console.log('error', error)
    }
    
  }

}
