import { Test, TestingModule } from '@nestjs/testing';

import { getModelToken } from 'nestjs-typegoose';

import { ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { MasterController } from './master.controller';
import { MasterService } from './master.service';
import { CountQueuingStrategy } from 'node:stream/web';
import { Countries } from 'src/models/countries/countries.model';

describe('MasterController', () => {
  let masterController: MasterController
  let masterService:MasterService

  const countriesArray : Countries[] = [
    {
      code: 'AF',
      name: 'Afghanistan',
    },
    {
      code: 'AX',
      name: 'Aland Islands',
    },
    {
      code: 'AL',
      name: 'Albania',
    }
  ]

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MasterController],
      providers: [
        {
          provide: MasterService,
          useValue: {
            getCountries: jest.fn()
          },
        },
      ],
    }).compile();

    masterController = app.get<MasterController>(MasterController);
    masterService = app.get<MasterService>(MasterService);
  })

  describe('getCountries()', () => {

    it('should return an array of Countries', async () => {

      const getCountriesSpy =jest.spyOn(masterService, 'getCountries').mockImplementationOnce(() =>
        Promise.resolve(countriesArray as any)
      );

      let countries = masterController.getCountries()
      expect(countries).resolves.toEqual([
        {
          code: 'AF',
          name: 'Afghanistan',
        },
        {
          code: 'AX',
          name: 'Aland Islands',
        },
        {
          code: 'AL',
          name: 'Albania',
        }
      ])
      expect(getCountriesSpy).toHaveBeenCalled()

    })
    
  })

  
})
