FROM node:21-slim

WORKDIR /

COPY package*.json ./

RUN npm install --save --legacy-peer-deps

COPY . .

EXPOSE 9910

CMD ["npm", "run", "start:dev"]